//[SECTION] Dependencies and Modules
	const User = require("../models/User");
	const Course = require("../models/Course");
	const bcrypt = require('bcrypt');
	const auth = require("../auth");


//[SECTION] Check if the email already exists

	module.exports.checkEmailExists = (reqBody) => {

		// The result is sent back to the frontend via the "then" method found in the route file
		return User.find({email : reqBody.email}).then(result => {

			// The "find" method returns a record if a match is found
			if (result.length > 0) {
				return true;

			// No duplicate email found
			// The user is not yet registered in the database
			} else {
				return false;
			}
		})
		.catch(err => res.send(err))
	};


//[SECTION] User registration
	module.exports.registerUser = (reqBody) => {

		// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newUser = new User({
			firstName : reqBody.firstName,
			lastName : reqBody.lastName,
			email : reqBody.email,
			mobileNo : reqBody.mobileNo,
			password : bcrypt.hashSync(reqBody.password, 10)
		})

		// Saves the created object to our database
		return newUser.save().then((user, error) => {

			// User registration failed
			if (error) {
				return false;

			// User registration successful
			} else {
				return true;
			}
		})
		.catch(err => err)
	};


//[SECTION] User authentication

	module.exports.loginUser = (req, res) => {
		User.findOne({ email : req.body.email} ).then(result => {

			console.log(result);

			// User does not exist
			if(result == null){

				return res.send(false);

			// User exists
			} else {
				
				const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
				// If the passwords match/result of the above code is true
				if (isPasswordCorrect) {
	
					return res.send({ access : auth.createAccessToken(result) })

				// Passwords do not match
				} else {

					return res.send(false);
				}
			}
		})
		.catch(err => res.send(err))
	};




//[SECTION] Retrieve user details

	module.exports.getProfile = (req, res) => {

		return User.findById(req.user.id)
		.then(result => {
			result.password = "";
			return res.send(result);
		})
		.catch(err => res.send(err))
	};



//[SECTION] Enroll a registered User

	module.exports.enroll = async (req, res) => {

	  	console.log(req.user.id) //the user's id from the decoded token after verify()
	  	console.log(req.body.courseId) //the course from our request body

	  	//process stops here and sends response IF user is an admin
	  	if(req.user.isAdmin){
	    	return res.send("Action Forbidden")
	  	}


	  	let isUserUpdated = await User.findById(req.user.id).then(user => {
	    let newEnrollment = {
	        courseId: req.body.courseId,
	        courseName: req.body.courseName,
	        courseDescription: req.body.courseDescription,
	        coursePrice: req.body.coursePrice
	    }
		    user.enrollments.push(newEnrollment);
		    
		    return user.save().then(user => true).catch(err => err.message)
	  	})
	  

	  	if(isUserUpdated !== true) {
	      	return res.send({message: isUserUpdated})
	  	}

		let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

		    let enrollee = {
		        userId: req.user.id
		    }

		    course.enrollees.push(enrollee);

	    	return course.save().then(course => true).catch(err => err.message)
	 	})


	 	if(isCourseUpdated !== true) {
	      	return res.send({ message: isCourseUpdated})
	  	}


	  	if(isUserUpdated && isCourseUpdated) {
	      	return res.send({ message: "Enrolled Successfully."})
	  	}
	}

	//[ACTIVITY] Getting user's enrolled courses
	module.exports.getEnrollments = (req, res) => {
	    User.findById(req.user.id)
	    .then(result => res.send(result.enrollments))
	    .catch(err => res.send(err))
	}







