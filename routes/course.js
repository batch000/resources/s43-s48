//[SECTION] Dependencies and Modules
	const express = require('express');
	const courseController = require("../controllers/course");
	const auth = require("../auth") 

	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
	const router = express.Router();


//[ACTIVITY] create a course POST
	router.post("/", verify, verifyAdmin, courseController.addCourse);


//[SECTION] Route for retrieving all the courses (Admin)
	router.get("/all", verify, verifyAdmin, courseController.getAllCourses);

//[SECTION] Route for retrieving all the ACTIVE courses for all users
	// Middleware for verifying JWT is not required because users who aren't logged in should also be able to view the courses
	router.get("/", courseController.getAllActive);

//[SECTION] Route for retrieving a specific course
	router.get("/:courseId", courseController.getCourse);

//[SECTION] Route for updating a course (Admin)
	router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);
	
//[ACTIVITY] Route to archiving a course (Admin)
	router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

//[ACTIVITY] Route to activating a course (Admin)
	router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);





// Allows us to export the "router" object that will be accessed in our "index.js" file
		module.exports = router;

